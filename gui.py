from tkinter import *
from nltk.corpus import wordnet as wn
import bs4 as bs
import urllib.request
import lxml
import re
import wikipedia
from tkinter import filedialog

def msearch():
  file = filedialog.askopenfilename()     
  open_file = open(file, 'r')             
  print(open_file.read())                 
  open_file.close()                       

root = Tk()
root.title("Wikipedia Based Question Generator")

txt_Input = StringVar()
operator=""
input_Data=""

txt_Copy = StringVar()
input_Copy=""
input_Copy=""

Tops = Frame(root, width=1600, height=738, bg="#054C56", relief=SUNKEN, bd=10)
Tops.pack(side=TOP, anchor=N)

Tops.pack_propagate(0) 

f1 = Frame(root, width=800, height=60, bg ="#C87637", relief=FLAT)
f1.pack(side=LEFT)

f1.pack_propagate(0) 

f2 = Frame(root, width=800, height=50, bg ="#C87637", relief=FLAT)
f2.pack(side=RIGHT)

f2.pack_propagate(0) 

txtDisplay1 = Entry(f1, font=('arial', 25), textvariable = txt_Input, bd=5, insertwidth=2, bg="#7DD8A0", justify='right')
txtDisplay1.grid(row=1, column=1)

txtDisplay2 = Entry(f2, font=('arial', 25), textvariable = txt_Copy, bd=5, insertwidth=4, bg="#7DD8A0", justify='left')
txtDisplay2.grid(row=1, column=1)

def btn1Click():
  def beautifulsoup(body):
    sauce = urllib.request.urlopen('https://en.wikipedia.org/wiki/' + txt_Input).read()
    soup = bs.BeautifulSoup(sauce, 'lxml')
    body = soup.body
    for paragraph in body.find_all('p'):
      print(paragraph.text)
  

 
  

btn1 = Button(f1, padx=9, pady=9, fg="#2E002E", font=('arial', 18, 'bold'), text="Search on Wikipedia", bg="#C5DACD", command = lambda: btn1Click()).grid(row=1, column=2)

btn2 = Button(f2, padx=9, pady=9, fg="#2E002E", bg="#C5DACD", font=('arial', 18, 'bold'), text="Import Text",command = lambda: msearch()).grid(row=1, column=2)

root.mainloop()